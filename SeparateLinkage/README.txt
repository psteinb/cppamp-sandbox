This directory contains an attempt to achieve separate compilation of MCW C++AMP codes

* Compile sources. Each one generates a .o 

SeparateLinkage$ clang++ `clamp-config --cxxflags` -c -o helloworld.o helloworld.cc 
SeparateLinkage$ clang++ `clamp-config --cxxflags` -c -o extlib.o extlib.cc

* Link objects and produce the final executable

SeparateLinkage$ clang++ `clamp-config --ldflags` -o test.out extlib.o helloworld.o

* Run it!
SeparateLinkage$ ./test.out
Hello world
